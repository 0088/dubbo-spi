package org.levi.demo;

import org.apache.dubbo.common.extension.ExtensionLoader;
import org.levi.demo.spi.service.HelloService;

import java.util.Set;

public class DubboSpiTest {

    public static void main(String[] args) {
        ExtensionLoader<HelloService> extensionLoader = ExtensionLoader.getExtensionLoader(HelloService.class);
        Set<String> loadedExtensions = extensionLoader.getSupportedExtensions();
        for (String loadedExtension : loadedExtensions) {
            extensionLoader.getExtension(loadedExtension).sayHello("tong tong");
        }

    }
}
