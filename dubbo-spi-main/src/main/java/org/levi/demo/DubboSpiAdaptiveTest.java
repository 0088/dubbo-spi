package org.levi.demo;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.levi.demo.spi.service.HelloService;

import java.util.Set;

public class DubboSpiAdaptiveTest {

    public static void main(String[] args) {
        URL url = URL.valueOf("localhost://test?hello.service=human");
        ExtensionLoader<HelloService> extensionLoader = ExtensionLoader.getExtensionLoader(HelloService.class);
        HelloService adaptiveExtension = extensionLoader.getAdaptiveExtension();
        adaptiveExtension.sayHello(url,"nicole");


    }
}
