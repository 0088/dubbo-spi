package org.levi.demo.spi.service.impl;

import org.apache.dubbo.common.URL;
import org.levi.demo.spi.service.HelloService;

public class DogHelloService implements HelloService {
    @Override
    public void sayHello(String name) {
        System.out.println("wang wang~~" + name);
    }

    @Override
    public void sayHello(URL url,String name) {
        System.out.println("dog " + url + name);
    }
}
