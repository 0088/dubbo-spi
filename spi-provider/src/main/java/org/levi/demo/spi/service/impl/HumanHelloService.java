package org.levi.demo.spi.service.impl;

import org.apache.dubbo.common.URL;
import org.levi.demo.spi.service.HelloService;

public class HumanHelloService implements HelloService {
    public void sayHello(String name) {
        System.out.println("hello " + name + ",nice to meet you dubbo");
    }

    @Override
    public void sayHello(URL url,String name) {
        System.out.println("human " + url + name);
    }
}
