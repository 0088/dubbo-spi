package org.levi.demo.spi.service;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.Adaptive;
import org.apache.dubbo.common.extension.SPI;

@SPI("human")

public interface HelloService {

    void sayHello(String name);
    @Adaptive
    void sayHello(URL url,String name);
}
